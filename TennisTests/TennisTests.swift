//
//  TennisTests.swift
//  TennisTests
//
//  Created by Mario Rotz on 15.11.16.
//  Copyright © 2016 Tennis. All rights reserved.
//

import XCTest
import Tennis

@testable import Tennis

class TennisTests: XCTestCase {
    var tennis : Tennis!
    override func setUp() {
        tennis = Tennis()
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testPlay_Love_All() {
        let result = tennis.play(playerOne: 0, playerTwo: 0)
        XCTAssertEqual(result,"Love-All")
    }
    
    func testPlay_Love_Fifteen() {
        let result = tennis.play(playerOne: 0, playerTwo: 1)
        XCTAssertEqual(result,"Love-Fifteen")
    }
    
    func testPlay_Fifteen_All() {
        let result = tennis.play(playerOne: 1, playerTwo: 1)
        XCTAssertEqual(result,"Fifteen-All")
    }
    
    func testPlay_Fifteen_Thirty() {
        let result = tennis.play(playerOne: 1, playerTwo: 2)
        XCTAssertEqual(result,"Fifteen-Thirty")
    }
    
    func testPlay_Fifteen_Forty() {
        let result = tennis.play(playerOne: 1, playerTwo: 3)
        XCTAssertEqual(result,"Fifteen-Forty")
    }
    
    func testPlay_Win_Player2() {
        var result = tennis.play(playerOne: 1, playerTwo: 4)
        XCTAssertEqual(result,"Win for Player 2")
        result = tennis.play(playerOne: 4, playerTwo: 6)
        XCTAssertEqual(result,"Win for Player 2")
    }
    
    func testPlay_Thirty_Forty(){
        let result = tennis.play(playerOne: 2, playerTwo: 3)
        XCTAssertEqual(result,"Thirty-Forty")
    }
    
    func testPlay_Deuce(){
        var result = tennis.play(playerOne: 3, playerTwo: 3)
        XCTAssertEqual(result,"Deuce")
        result = tennis.play(playerOne: 4, playerTwo: 4)
        XCTAssertEqual(result,"Deuce")
    }
    
    func testPlay_Advantage_PlayerTwo(){
        let result = tennis.play(playerOne: 3, playerTwo: 4)
        XCTAssertEqual(result,"Advantage for Player 2")
    }
    
    func testPlay_Advantage_PlayerOne(){
        let result = tennis.play(playerOne: 5, playerTwo: 4)
        XCTAssertEqual(result,"Advantage for Player 1")
    }
    
    func testPlay_Win_PlayerTwo(){
        var result = tennis.play(playerOne: 6, playerTwo: 4)
        XCTAssertEqual(result,"Win for Player 1")
        result = tennis.play(playerOne: 4, playerTwo: 1)
        XCTAssertEqual(result,"Win for Player 1")
    }
}
