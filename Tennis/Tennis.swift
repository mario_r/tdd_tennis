//
//  Tennis.swift
//  Tennis
//
//  Created by Mario Rotz on 15.11.16.
//  Copyright © 2016 Tennis. All rights reserved.
//

import UIKit

class Tennis: NSObject {
    var dict = [0:"Love", 1:"Fifteen", 2:"Thirty", 3:"Forty", 4: "undefined"]
    
    func play(playerOne:Int,playerTwo:Int) -> String
    {
        if let retVal = winPlayer1(playerOne: playerOne,playerTwo: playerTwo){
            return retVal
        }
        
        if let retVal = winPlayer2(playerOne: playerOne,playerTwo: playerTwo){
            return retVal
        }
        
        if let retVal = advantageOrDeuce(playerOne:playerOne, playerTwo:playerTwo) {
            return retVal
        }
        
        if let retVal = countMatch(playerOne:playerOne,playerTwo:playerTwo)
        {
            return retVal
        }
        return ""
    }
    
    func winPlayer1(playerOne:Int,playerTwo:Int) -> String?
    {
        if playerOne>3 && playerOne>playerTwo+1  {
            return "Win for Player 1"
        }
        else
        {
            return nil
        }
    }
    
    func winPlayer2(playerOne:Int,playerTwo:Int) -> String?
    {
        if playerTwo>3 && playerTwo>playerOne+1 {
            return "Win for Player 2"
        }
        else
        {
            return nil
        }
    }
    
    func advantageOrDeuce(playerOne:Int,playerTwo:Int) -> String?
    {
        if playerOne>2 && playerTwo>2 {
            if playerOne==playerTwo+1 {
                return "Advantage for Player 1"
            }
            if playerTwo==playerOne+1 {
                return "Advantage for Player 2"
            }
            if playerOne==playerTwo {
                return "Deuce"
            }
        }
        return nil
    }
    
    func countMatch(playerOne:Int, playerTwo:Int) -> String?
    {
        if playerOne==playerTwo && playerOne<4 {
            return dict[playerOne]! + "-All"
        }
        
        if playerOne<playerTwo && playerOne<4 && playerTwo<4 {
            return dict[playerOne]! + "-" + dict[playerTwo]!
        }
        return nil
    }
    
}
